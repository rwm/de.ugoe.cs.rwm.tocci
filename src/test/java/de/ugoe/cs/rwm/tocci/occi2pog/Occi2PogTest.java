/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.tocci.occi2pog;

import static org.junit.Assert.assertTrue;

import java.lang.reflect.InvocationTargetException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.eclipse.epsilon.emc.emf.CachedResourceSet;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import de.ugoe.cs.rwm.tocci.TestUtility;
import de.ugoe.cs.rwm.tocci.Transformator;
import de.ugoe.cs.rwm.tocci.TransformatorFactory;

public class Occi2PogTest {
	@BeforeClass
	public static void OCCIRegistrySetup() {
		TestUtility.extensionRegistrySetup();
		Logger.getLogger(Transformator.class.getName()).setLevel(Level.DEBUG);
	}

	@Test
	public void createPOG() throws EolRuntimeException {
		Path occiPath = Paths.get(TestUtility.getPathToResource("occi/PaaS.occic"));
		org.eclipse.emf.ecore.resource.Resource occi = TestUtility.loadOCCIResource(occiPath);

		Transformator occiToPog = TransformatorFactory.getTransformator("OCCI2POG");
		Path outputpath = Paths.get(TestUtility.getPathToResource("pog/POG.pog"));
		occiToPog.transform(occi, outputpath);
		assertTrue(true);
	}

	@Test
	public void createWaaSPOG() throws EolRuntimeException {
		Path occiPath = Paths.get(TestUtility.getPathToResource("occi/WaaS.occic"));
		org.eclipse.emf.ecore.resource.Resource occi = TestUtility.loadOCCIResource(occiPath);

		Transformator occiToPog = TransformatorFactory.getTransformator("OCCI2POG");
		Path outputpath = Paths.get(TestUtility.getPathToResource("pog/POG.pog"));
		occiToPog.transform(occi, outputpath);
		assertTrue(true);
	}

	@Test
	public void createExternalExtensionPOG() throws EolRuntimeException {
		Path occiPath = Paths.get(TestUtility.getPathToResource("occi/WaaSWithApp.occic"));
		org.eclipse.emf.ecore.resource.Resource occi = TestUtility.loadOCCIResource(occiPath);

		Transformator occiToPog = TransformatorFactory.getTransformator("OCCI2POG");
		Path outputpath = Paths.get(TestUtility.getPathToResource("pog/POG.pog"));
		occiToPog.transform(occi, outputpath);
		assertTrue(true);
	}

	@Test
	public void controlFlowLink() throws EolRuntimeException {
		Path occiPath = Paths.get(TestUtility.getPathToResource("occi/MLSnewDiffVM2.occic"));
		org.eclipse.emf.ecore.resource.Resource occi = TestUtility.loadOCCIResource(occiPath);

		Transformator occiToPog = TransformatorFactory.getTransformator("OCCI2POG");
		Path outputpath = Paths.get(TestUtility.getPathToResource("pog/POG.pog"));
		occiToPog.transform(occi, outputpath);
		assertTrue(true);
	}

	@Ignore("To fix: Link source of typical Link seems to be empty (But only after a migration).")
	@Test
	public void typicalLink() throws EolRuntimeException, InvocationTargetException {
		Path occiPath = Paths.get(TestUtility.getPathToResource("occi/clusterTypicalLink.occic"));
		Transformator trans = TransformatorFactory.getTransformator("OCCI2OCCI");
		trans.transform(occiPath, occiPath);
		CachedResourceSet.getCache().clear();
		org.eclipse.emf.ecore.resource.Resource occi = TestUtility.loadOCCIResource(occiPath);

		Transformator occiToPog = TransformatorFactory.getTransformator("OCCI2POG");
		Path outputpath = Paths.get(TestUtility.getPathToResource("pog/POG.pog"));
		occiToPog.transform(occi, outputpath);
		assertTrue(true);
	}
}
