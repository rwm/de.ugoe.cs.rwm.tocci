/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.tocci.prod2local;

import de.ugoe.cs.rwm.cocci.Comparator;
import de.ugoe.cs.rwm.cocci.ComparatorFactory;
import de.ugoe.cs.rwm.cocci.ModelUtility;
import de.ugoe.cs.rwm.tocci.TestUtility;
import de.ugoe.cs.rwm.tocci.Transformator;
import de.ugoe.cs.rwm.tocci.design2reqruntime.DESIGN2REQRUNTIMETransformator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.eclipse.cmf.occi.core.Configuration;
import org.eclipse.cmf.occi.core.MixinBase;
import org.eclipse.cmf.occi.core.Resource;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.epsilon.emc.emf.CachedResourceSet;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.assertTrue;

public class Prod2LocalTest {
	Path targetOcci = Paths.get(TestUtility.getPathToResource("occi/Target.occic"));

	@BeforeClass
	public static void OCCIRegistrySetup() {
		TestUtility.extensionRegistrySetup();
		Logger.getLogger(Comparator.class.getName()).setLevel(Level.INFO);
		Logger.getLogger(Comparator.class.getName()).setLevel(Level.FATAL);
	}

	@Before
	public void clearCache() {
		CachedResourceSet.getCache().clear();
	}

	@Test
	public void annotationTest() throws EolRuntimeException {
		Logger.getLogger(Transformator.class.getName()).setLevel(Level.DEBUG);

		Path designOcci = Paths
				.get(TestUtility.getPathToResource("prod/hadoop.occic"));
		PROD2LOCALTransformator trans = new PROD2LOCALTransformator();
		trans.transform(designOcci, targetOcci);

		// clearCache();
		org.eclipse.emf.ecore.resource.Resource finishedModel = TestUtility.loadOCCIResource(targetOcci);
		Configuration config = (Configuration) finishedModel.getContents().get(0);

		boolean simMixfound = false;
		for(Resource res: config.getResources()){
			for(MixinBase mixB: res.getParts()){
				System.out.println(mixB.getMixin());
				if(mixB.getMixin().getTerm().contains("simulation")){
					simMixfound = true;
				}
			}
		}
		assertTrue(simMixfound);
		System.out.println(finishedModel.getContents().get(0));
		System.out.println(targetOcci.toString());

	}


	@Test
	public void annotationMonitorTest() throws EolRuntimeException {
		Logger.getLogger(Transformator.class.getName()).setLevel(Level.DEBUG);

		Path designOcci = Paths
				.get(TestUtility.getPathToResource("prod/Hadoop_Full_Thesis.occic"));
		PROD2LOCALTransformator trans = new PROD2LOCALTransformator();
		trans.transform(designOcci, targetOcci);

		// clearCache();
		org.eclipse.emf.ecore.resource.Resource finishedModel = TestUtility.loadOCCIResource(targetOcci);
		Configuration config = (Configuration) finishedModel.getContents().get(0);

		boolean simMixfound = false;
		for(Resource res: config.getResources()){
			for(MixinBase mixB: res.getParts()){
				System.out.println(mixB.getMixin());
				if(mixB.getMixin().getTerm().contains("simulation")){
					simMixfound = true;
				}
			}
		}
		assertTrue(simMixfound);
		System.out.println(finishedModel.getContents().get(0));
		System.out.println(targetOcci.toString());

	}
}
