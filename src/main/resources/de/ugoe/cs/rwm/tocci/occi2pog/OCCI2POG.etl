/* Operation: getDependsOnConfiguration
Description: Configuration Set to map OCCI Kinds to their corresponding dependency resolution.*/
operation getDependsOnConfiguration() : Set {
	var configuration = new Set;
	return configuration;
}
/* Operation: getUseConfiguration
Description: Configuration Set to map OCCI Kinds to their corresponding dependency resolution.*/
operation getUseConfiguration() : Set {
	var configuration = new Set;
	configuration.add(Kind("componentlink", "http://schemas.ogf.org/occi/platform#", "component link resource"));
	configuration.add(Kind("networkinterface", "http://schemas.ogf.org/occi/infrastructure#", "network link resource"));
	configuration.add(Kind("storagelink", "http://schemas.ogf.org/occi/infrastructure#", "storage link resource"));
	configuration.add(Kind("componentlink", "http://schemas.modmacao.org/occi/platform#", "component link"));
	configuration.add(Kind("placementlink", "http://schemas.modmacao.org/placement#", "placement link"));
	return configuration;
}


pre {
    var trans = new Native("de.ugoe.cs.rwm.tocci.occi2pcg.OCCI2PCGTransformator");
	trans.logInfo("Starting OCCI2POG Transformation...");
	var useDependencies = getUseConfiguration();
	var dependsOnDependencies = getDependsOnConfiguration();
	
	var linkVertexTitles = new Set();
	var resourceVertexTitles = new Set();
	
	var graph = new POG!Graph();
}
post {
	createEdges();	
	trans.logInfo("Resource Vertexes Created: " + printTitleSet(resourceVertexTitles));
	trans.logInfo("Link Vertexes Created: " + printTitleSet(linkVertexTitles));
 	trans.logInfo("Transformation Process was Successfull");
}

//-----------------------------------------------//
//------------------Link2Vertex------------------//
//-----------------------------------------------//
@greedy
rule Link2Vertex
	transform link : OCCI!Link
	to vertex : POG!Vertex {
	vertex.id = link.id;
	
	trans.logDebug("Link: " + link);
    
	vertex.title = "(";
	if(link.title <> ""){
		vertex.title += link.title;
	}
	else{
		vertex.title += link.kind.term;
	}
	
	vertex.title += " ";

	trans.logDebug("Link source: " + link.source);
	if(link.source.title <> "") {
		vertex.title += link.source.title;
	}
	else {
		vertex.title += link.source.kind.term;
	}
	
	vertex.title += " -> ";
	

	trans.logDebug("Link target: " + link.target);
	if(link.target.title <> "") {
		vertex.title += link.target.title;
	}
	else{
		vertex.title += link.target.kind.term;
	}
	vertex.title += ")";
	
	trans.logDebug("Created Link Vertex: " + vertex.title);	
	linkVertexTitles.add(vertex.title);
	graph.vertices.add(vertex);
}

//-----------------------------------------------//
//--------------Resource2Vertex------------------//
//-----------------------------------------------//
@greedy
rule Resource2Vertex
	transform resource : OCCI!Resource
	to vertex : POG!Vertex {
	vertex.id = resource.id;
	if(resource.title <> ""){
		vertex.title = resource.title;
	}
	else{
		vertex.title = resource.kind.term;
	}
	trans.logDebug("Created Resource Vertex: " + vertex.title);
	resourceVertexTitles.add(vertex.title);
	graph.vertices.add(vertex);
}

operation printTitleSet(set : Set) : String {
    var str = "";
    for(title in set) {
        str += title;
        str += " | ";
    }
return str;
}


/* Operation: Kind
Input:	String term: Term of the Kind,
		String scheme: Scheme of the Kind,
		String title: Title of the Kind.
Description: Simple creation of a Kind Instance.*/
operation Kind(term : String, scheme : String, title : String) : OCCI!Kind{
	var kind = new OCCI!Kind;
	kind.name = term;
	kind.title = title;
	kind.scheme = scheme;
	
	return kind;
}

/* Operation: baseKind
Description: Search for the root parent of the Kind passed. 
To be in more Detail the previous parent before the root is examined,
because all OCCI Kinds are inherited from link,resource or entity kind,
which contain no valuable information.*/
operation OCCI!Kind baseKind() : OCCI!Kind {
	if(self.parent.scheme == "http://schemas.ogf.org/occi/core#"){
		return self;
	}
	else{
	 self.parent.baseKind();
	}
}
/* Operation: compareTo
Input:	Kind kind: Kind that is compared against.
Description: Compares an OCCI Kind to another OCCI Kind
based on the mandatory properties scheme and term.*/
operation OCCI!Kind compareTo(kind : OCCI!Kind) : Boolean {
trans.logDebug("Comparing: " + self + " to " + kind);
    //"Comparing: ".print();
    //self.print();
    //" to ".print();
    //kind.println();
	if(self.term.toString() == kind.term.toString()
	and self.scheme.toString() == kind.scheme.toString()) {
		return true;
	}
	return false;
}

/* Operation: createEdges
Description: For every Link in the OCCI Model, the baseKind of the Links Kind is checked.
Depending on the configuration file either a use or depends on relationship is associated to the Kind.
Thereupon, Edges are created in the POG linking corresponding Vertices.*/

operation createEdges() {
	for(link in OCCI!Link){
		var linkBaseKind = new String;
		if(dependsOnDependencies.exists(kind | kind.compareTo(link.kind))){
			Edge(link.target.equivalent(), link.equivalent(), graph);
			Edge(link.equivalent(), link.source.equivalent(), graph);
		}
		else if(useDependencies.exists(kind | kind.compareTo(link.kind))){
			Edge(link.target.equivalent(), link.equivalent(), graph);
			Edge(link.source.equivalent(), link.equivalent(), graph);
		}
		else{
		  trans.logWarn("Kind: " + link.kind.title + " not registered. Using Use Dependency as Default!");
		   Edge(link.target.equivalent(), link.equivalent(), graph);
           Edge(link.source.equivalent(), link.equivalent(), graph);
		}
	}
}

/* Operation: Edge
Input:	Vertex source: Source of the Edge,
		Vertex target: Target of the Edge,
		Graph graph: Graph the Edge is contained in.
Description: Simple creation of an Edge.*/
operation Edge(source : POG!Vertex, target : POG!Vertex, graph: POG!Graph) {
	var edge = new POG!Edge;
	edge.source = source;
	edge.target = target;
	var str = "Create Edge: ";
	if(source.title <> null){
		str += source.title;
	}
	else {
		str += source.id;
	}
	str += " -> ";
	if(target.title <> null) {
		str += target.title;
	}
	else{
		str += target.id;
	}
	
	trans.logDebug(str);

	graph.edges.add(edge);
}