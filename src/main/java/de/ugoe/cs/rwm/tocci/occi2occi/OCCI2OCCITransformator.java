/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.tocci.occi2occi;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.epsilon.emc.emf.InMemoryEmfModel;
import org.eclipse.epsilon.eol.IEolModule;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.eclipse.epsilon.eol.models.IModel;

import de.ugoe.cs.rwm.tocci.AbsTransformator;

/**
 * Class implementing the OCCI2POG transformation.
 *
 * @author Johannes Erbel
 *
 */
public class OCCI2OCCITransformator extends AbsTransformator {
	private final static File EOLFILE = AbsTransformator.getTransFile("de/ugoe/cs/rwm/tocci/occi2occi/OCCI2OCCI.eol");

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.ugoe.cs.oco.occi2deployment.transformation.Transformator#transform(java.
	 * nio.file.Path, java.nio.file.Path)
	 */
	public String transform(Path occiModelPath, Path newocciModelPath) throws EolRuntimeException {
		try {
			Files.copy(occiModelPath, newocciModelPath, StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		IEolModule module = eolModuleSetup(EOLFILE);

		String occiURI = "http://schemas.ogf.org/occi/core/ecore";

		IModel newocciModel;
		try {
			newocciModel = createEmfModel("OCCI", newocciModelPath.toAbsolutePath().toString(), occiURI, true, true);
			module.getContext().getModelRepository().addModel(newocciModel);
			module.execute();

			newocciModel.store();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.ugoe.cs.oco.occi2deployment.transformation.Transformator#transform(java.
	 * nio.file.Path, java.nio.file.Path, java.nio.file.Path)
	 */
	public String transform(Path inputPath, Path outputPath, Path additionalPath) throws EolRuntimeException {
		return transform(inputPath, outputPath);
	}

	@Override
	public String transform(Resource inputModel, Path outputPath) throws EolRuntimeException {
		IEolModule module = eolModuleSetup(EOLFILE);

		InMemoryEmfModel occiModel = new InMemoryEmfModel("OCCI", inputModel);
		// occiModel.getAliases().add("OCCI");

		module.getContext().getModelRepository().addModel(occiModel);

		module.execute();
		occiModel.store();
		// TODO: Store OCCI Configuration
		return null;
	}

	@Override
	public String transform(Resource sourceModel, Resource targetModel, Path outputPath) throws EolRuntimeException {
		return transform(sourceModel, outputPath);
	}
}
