/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/
/**
 * Package containing the general Transformation interface.
 *
 * @since 1.0
 * @author Johannes Erbel
 * @version 1.0
 */

package de.ugoe.cs.rwm.tocci;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.util.Collection;
import java.util.Map;
import java.util.UUID;

import org.eclipse.cmf.occi.core.OCCIPackage;
import org.eclipse.cmf.occi.core.util.OCCIResourceFactoryImpl;
import org.eclipse.cmf.occi.core.util.OcciRegistry;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.epsilon.common.parse.problem.ParseProblem;
import org.eclipse.epsilon.common.util.StringProperties;
import org.eclipse.epsilon.emc.emf.EmfModel;
import org.eclipse.epsilon.eol.EolModule;
import org.eclipse.epsilon.eol.IEolModule;
//import org.eclipse.epsilon.eol.IEolExecutableModule;
import org.eclipse.epsilon.eol.exceptions.models.EolModelLoadingException;
import org.eclipse.epsilon.eol.models.IRelativePathResolver;
import org.eclipse.epsilon.etl.EtlModule;
import org.eclipse.epsilon.flock.FlockModule;

/**
 * Class containing the method to create an EMF Model, required for the run
 * configuration of ETL transformations.
 *
 * @author Johannes Erbel
 */
public abstract class AbsTransformator implements Transformator {

	/**
	 * Returns Transformation File, either contained in the Resource folder or
	 * packed jar.
	 *
	 * @param resourceName
	 *            Name of the transformation file.
	 * @return The transformation file at the specified location.
	 */
	public static File getTransFile(String resourceName) {
		File file = null;
		try {
			file = File.createTempFile("tempfile", ".tmp");
		} catch (IOException e) {
			LOG.error("Tempfile for Transformation could not be created!");
			e.printStackTrace();
		}
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		try (InputStream input = classLoader.getResourceAsStream(resourceName);
				OutputStream out = new FileOutputStream(file)) {
			int read;
			int byteArrSize = 1024;
			byte[] bytes = new byte[byteArrSize];

			while ((read = input.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			}
			file.deleteOnExit();

		} catch (IOException ex) {
			ex.printStackTrace();
		}
		if (file == null || file.exists() == false) {
			throw new RuntimeException("Error: Transformationfile " + resourceName + " not found!");
		}

		return file;
	}

	protected FlockModule flockModuleSetup(File flockFile) {
		FlockModule module = new FlockModule();
		try {
			module.parse(flockFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (module.getParseProblems().size() > 0) {
			System.err.println("Parse errors occured...");
			for (ParseProblem problem : module.getParseProblems()) {
				System.err.println(problem.toString());
			}
		}
		return module;
	}

	protected IEolModule etlModuleSetup(File etlFile) {
		IEolModule module = new EtlModule();
		try {
			module.parse(etlFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (module.getParseProblems().size() > 0) {
			System.err.println("Parse errors occured...");
			for (ParseProblem problem : module.getParseProblems()) {
				System.err.println(problem.toString());
			}
		}
		return module;
	}

	protected IEolModule eolModuleSetup(File eolFile) {
		IEolModule module = new EolModule();
		try {
			module.parse(eolFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (module.getParseProblems().size() > 0) {
			System.err.println("Parse errors occured...");
			for (ParseProblem problem : module.getParseProblems()) {
				System.err.println(problem.toString());
			}
		}
		return module;
	}

	/**
	 * Creates EMF Model, required for the run configuration of ETL transformations.
	 *
	 * @param name
	 *            Name of the EmfModel.
	 * @param uri
	 *            URI of the EmfModel.
	 * @param metauri
	 *            URI of the Metamodel.
	 * @param readOnLoad
	 *            Boolean for the read on load option.
	 * @param storeOnDisposal
	 *            Boolean for the store on disposal option.
	 * @return EMF model
	 * @throws EolModelLoadingException
	 *             Exception thrown when emfModel could not be loaded.
	 * @throws URISyntaxException
	 *             Exception thrown when the URI syntax of the model to be created
	 *             contains errors.
	 */
	protected EmfModel createEmfModel(String name, String uri, String metauri, boolean readOnLoad,
			boolean storeOnDisposal) throws EolModelLoadingException, URISyntaxException {
		EmfModel emfModel = new EmfModel();

		StringProperties properties = new StringProperties();
		if (metauri != null) {
			properties.put(EmfModel.PROPERTY_METAMODEL_URI, metauri);
		}

		properties.put(EmfModel.PROPERTY_EXPAND, "true");
		properties.put(EmfModel.PROPERTY_NAME, name);
		properties.put(EmfModel.PROPERTY_MODEL_URI, uri);
		properties.put(EmfModel.PROPERTY_READONLOAD, readOnLoad + "");
		properties.put(EmfModel.PROPERTY_STOREONDISPOSAL, storeOnDisposal + "");
		emfModel.getAliases().add(name);
		emfModel.load(properties, (IRelativePathResolver) null);
		return emfModel;
	}

	protected EmfModel createEmfModel(String name, org.eclipse.emf.common.util.URI uri, String pcgURI,
			boolean readOnLoad, boolean storeOnDisposal) throws EolModelLoadingException, URISyntaxException {
		EmfModel emfModel = new EmfModel();

		StringProperties properties = new StringProperties();
		if (pcgURI != null) {
			properties.put(EmfModel.PROPERTY_METAMODEL_URI, pcgURI);
		}

		properties.put(EmfModel.PROPERTY_EXPAND, "true");
		properties.put(EmfModel.PROPERTY_NAME, name);
		properties.put(EmfModel.PROPERTY_MODEL_URI, uri);
		properties.put(EmfModel.PROPERTY_READONLOAD, readOnLoad + "");
		properties.put(EmfModel.PROPERTY_STOREONDISPOSAL, storeOnDisposal + "");
		emfModel.getAliases().add(name);
		emfModel.load(properties, (IRelativePathResolver) null);
		return emfModel;
	}

	protected static String getRegisteredOCCIEcoreUris() {
		Collection<String> extensions = OcciRegistry.getInstance().getRegisteredExtensions();
		StringBuilder allUris = new StringBuilder();
		for (String extension : extensions) {
			String replaced = extension.replace("#", "/ecore");
			allUris.append(replaced);
			allUris.append(", ");
		}
		return allUris.toString();

	}

	public static Resource loadOCCIintoEMFResource(Path configuration) {
		OCCIPackage.eINSTANCE.eClass();
		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;

		Map<String, Object> m = reg.getExtensionToFactoryMap();
		m.put("occie", new OCCIResourceFactoryImpl());
		m.put("occic", new OCCIResourceFactoryImpl());

		ResourceSet resSet = new ResourceSetImpl();

		String file = new File(configuration.toString()).getAbsolutePath();
		Resource resource = resSet.getResource(URI.createFileURI(file), true);

		EcorePlugin.ExtensionProcessor.process(null);
		EcoreUtil.resolveAll(resSet);

		return resource;
	}

	public void logInfo(String msg) {
		LOG.info(msg);
	}

	public void logDebug(String msg) {
		LOG.debug(msg);
	}

	public void logError(String msg) {
		LOG.error(msg);
	}

	public void logFatal(String msg) {
		LOG.fatal(msg);
	}

	public void logWarn(String msg) {
		LOG.warn(msg);
	}

	public String generateUUID() {
		UUID uuid = UUID.randomUUID();
		return uuid.toString();
	}
}
