/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.tocci.prod2local;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

import org.eclipse.cmf.occi.core.util.OcciRegistry;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.epsilon.emc.emf.InMemoryEmfModel;
import org.eclipse.epsilon.eol.IEolModule;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;

import de.ugoe.cs.rwm.tocci.AbsTransformator;
import org.eclipse.epsilon.etl.IEtlModule;

/**
 * Class implementing the OCCI2OCCIRUNTIMECONFIG transformation.
 *
 * @author Johannes Erbel
 *
 */
public class PROD2LOCALTransformator extends AbsTransformator {

	private final static File ETLFILE = AbsTransformator
			.getTransFile("de/ugoe/cs/rwm/tocci/prod2local/PROD2LOCAL.etl");

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.ugoe.cs.oco.occi2deployment.transformation.Transformator#transform(java.
	 * nio.file.Path, java.nio.file.Path)
	 */
	public String transform(Path occiModelPath, Path newocciModelPath) throws EolRuntimeException {
		try {
			Files.copy(occiModelPath, newocciModelPath, StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Resource res = loadOCCIintoEMFResource(newocciModelPath);
		transform(res, newocciModelPath);

		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.ugoe.cs.oco.occi2deployment.transformation.Transformator#transform(java.
	 * nio.file.Path, java.nio.file.Path, java.nio.file.Path)
	 */
	public String transform(Path inputPath, Path outputPath, Path additionalPath) throws EolRuntimeException {
		return transform(inputPath, outputPath);
	}

	@Override
	public String transform(Resource inputModel, Path outputPath) throws EolRuntimeException {
		IEolModule module = etlModuleSetup(ETLFILE);

		InMemoryEmfModel occiModel = new InMemoryEmfModel("OCCI", inputModel);

		ResourceSet extResourceSet = new ResourceSetImpl();
		URI sim = URI.createURI(
				OcciRegistry.getInstance().getExtensionURI("http://schemas.ugoe.cs.rwm/domain/workload#"));
		Resource simResource = extResourceSet.getResource(sim, true);
		InMemoryEmfModel simModel = new InMemoryEmfModel("Sim", simResource);

		URI docker = URI.createURI(
				OcciRegistry.getInstance().getExtensionURI("http://occiware.org/occi/docker#"));
		Resource dockerRes = extResourceSet.getResource(docker, true);
		InMemoryEmfModel dockerModel = new InMemoryEmfModel("Docker", dockerRes);

		URI infra = URI.createURI(
				OcciRegistry.getInstance().getExtensionURI("http://schemas.ogf.org/occi/infrastructure#"));
		Resource infraRes = extResourceSet.getResource(infra, true);
		InMemoryEmfModel infraModel = new InMemoryEmfModel("Infra", infraRes);

		module.getContext().getModelRepository().addModel(occiModel);
		module.getContext().getModelRepository().addModel(simModel);
		module.getContext().getModelRepository().addModel(dockerModel);
		module.getContext().getModelRepository().addModel(infraModel);

		module.execute();
		occiModel.setModelFile(outputPath.toString());
		occiModel.store();
		// TODO: Store OCCI Configuration at additionalPath
		return null;
	}

	@Override
	public String transform(Resource sourceModel, Resource targetModel, Path outputPath) throws EolRuntimeException {
		return transform(sourceModel, outputPath);
	}
}
