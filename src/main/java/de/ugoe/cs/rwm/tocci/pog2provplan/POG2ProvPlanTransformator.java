/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.tocci.pog2provplan;

import java.io.File;
import java.net.URISyntaxException;
import java.nio.file.Path;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.epsilon.eol.IEolModule;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.eclipse.epsilon.eol.models.IModel;
import org.eclipse.uml2.uml.UMLPackage;

import de.ugoe.cs.rwm.tocci.AbsTransformator;
import pog.PogPackage;

/**
 * Class implementing the POG2ProvPlan transformation.
 *
 * @author Johannes Erbel
 *
 */
public class POG2ProvPlanTransformator extends AbsTransformator {
	private final static File ETLFILE = AbsTransformator
			.getTransFile("de/ugoe/cs/rwm/tocci/pog2provplan/POG2ProvPlan.etl");

	public POG2ProvPlanTransformator() {
		PogPackage.eINSTANCE.eClass();
		UMLPackage.eINSTANCE.eClass();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.ugoe.cs.oco.occi2deployment.transformation.Transformator#transform(java.
	 * nio.file.Path, java.nio.file.Path)
	 */
	public String transform(Path pogModelPath, Path umlModelPath) throws EolRuntimeException {
		IEolModule module = etlModuleSetup(ETLFILE);
		try {

			URI uri = URI.createFileURI(pogModelPath.toString());
			String pogURI = "http://swe.simpaas.pog.de/pog";
			IModel pogModel = createEmfModel("POG", uri, pogURI, true, false);

			uri = URI.createFileURI(umlModelPath.toString());
			// Used 4.0.0 metamodel due to repository availability
			String umlURI = "http://www.eclipse.org/uml2/4.0.0/UML";
			IModel umlModel = createEmfModel("UML", uri, umlURI, false, true);

			module.getContext().getModelRepository().addModel(umlModel);
			module.getContext().getModelRepository().addModel(pogModel);

			Object result = module.execute();
			umlModel.store();
			return result.toString();

		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.ugoe.cs.oco.occi2deployment.transformation.Transformator#transform(java.
	 * nio.file.Path, java.nio.file.Path, java.nio.file.Path)
	 */
	public String transform(Path inputPath, Path outputPath, Path additionalPath) throws EolRuntimeException {
		return transform(inputPath, outputPath);
	}

	@Override
	public String transform(Resource inputModel, Path outputModel) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String transform(Resource sourceModel, Resource targetModel, Path outputPath) {
		// TODO Auto-generated method stub
		return null;
	}
}
